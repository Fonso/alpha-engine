#include <AlphaEngine.hpp>

class SandboxApplication : public AlphaEngine::Application
{
	AlphaEngine::Observer KeyDown; 

	public: 
		SandboxApplication  () 
		{
				 
		};
		~SandboxApplication () {};


		void Start() override
		{
			ALPHAENGINE_INFO("START");
			//Destruction is a problem 
			static AlphaEngine::KeyMessage m(65, AlphaEngine::KeyMessage::KeyState::KEYDOWN);
			AlphaEngine::MessageDispatcher::GetInstance()->RegisterObserver(&m, KeyDown); 

			//TODO Mirar el tema de los destructores y tal ->>>>>>Posible solucion, que cree y destruya mensajes el dispatcher. Asi sabemos que tenemos 
			//la referencia todo el rato y no se nos destryen cosas
		}

		void Update() override
		{
			ALPHAENGINE_INFO("Hello"); 
		}

};

AlphaEngine::Application* CreateAplication()
{
	return new SandboxApplication(); 
}

