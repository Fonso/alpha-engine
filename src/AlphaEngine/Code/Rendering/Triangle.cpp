#include <gtc/matrix_transform.hpp>
#include <Rendering/Triangle.hpp>

AlphaEngine::Triangle::Triangle(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale):
m_position(position), 
m_rotation(rotation), 
m_scale   (scale) 
{
}

AlphaEngine::Triangle::~Triangle()
{
}

glm::mat4 AlphaEngine::Triangle::GetModelMatrix()
{
    static float const_rot = 0.0f; 
    const_rot += 0.0001f;
    glm::mat4 traslation = glm::translate(glm::mat4(1.0f), m_position);
    //TODO Make real rotation system 
    glm::mat4 rotation   = glm::rotate(   glm::mat4(1.0f),glm::degrees(const_rot),  glm::vec3(0,2,0));
    glm::mat4 scale      = glm::scale (   glm::mat4(1.0f), m_scale);

    return traslation * rotation * scale;
}

