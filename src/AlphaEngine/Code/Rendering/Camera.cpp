#include <gtc/matrix_transform.hpp>
#include <Rendering/Camera.hpp>

AlphaEngine::Camera* AlphaEngine::Camera::m_activeCamera; 

AlphaEngine::Camera::Camera(float fov, float ratio, float near, float far, bool isOrtographic, glm::vec3 position, glm::vec3 rotation, bool isActive)
: m_fov(fov), m_ratio(ratio), m_near(near), m_far(far), m_ortographic(isOrtographic), m_position(position), m_rotation(rotation), m_active(isActive) 
{
	if(!m_activeCamera)
	{
		m_activeCamera = this; 
		m_active	   = true;
	}
}

AlphaEngine::Camera::~Camera()
{ 

}

bool AlphaEngine::Camera::isActive() const
{
	return m_active;
}

glm::mat4 AlphaEngine::Camera::GetPerspective()
{
	//TODO Implementar camara ortografica
	return glm::perspective(glm::radians(m_fov), m_ratio, m_near, m_far);
}

glm::mat4 AlphaEngine::Camera::GetView()
{ 
	return glm::lookAt(m_position, m_rotation, glm::vec3(0,1,0)); 
}

AlphaEngine::Camera& AlphaEngine::Camera::GetActiveCamera()
{
	return *Camera::m_activeCamera;
}




