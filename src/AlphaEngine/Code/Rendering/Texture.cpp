#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#include <Rendering/Texture.hpp>


void AlphaEngine::Texture::Bind(const char* TexturePath, int textureWidth, int textureHeight, int channel) 
{
	stbi_set_flip_vertically_on_load(true);
	unsigned char* data = stbi_load(TexturePath, &textureWidth, &textureHeight, &channel, 0);
	glEnable(GL_TEXTURE_2D);

	glGenTextures(1, &m_textureID);
	glBindTexture(GL_TEXTURE_2D, m_textureID);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	if (data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, textureWidth, textureHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	stbi_image_free(data);
}

AlphaEngine::Texture::Texture(const char* TexturePath, int textureWidth, int textureHeight, int channel)
{
	Bind(TexturePath, textureWidth, textureHeight, channel); 
}

GLuint& AlphaEngine::Texture::GetTextureID()
{
	// TODO: Insertar una instrucci�n "return" aqu�
	return m_textureID; 
}

AlphaEngine::Texture::~Texture()
{
}
