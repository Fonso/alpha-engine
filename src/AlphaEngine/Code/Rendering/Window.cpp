#include <Rendering/Window.hpp>

AlphaEngine::Window::Window(int width, int height, const char* name)
{

    if (!glfwInit())
    {
        fprintf(stderr, "Failed to initialize GLFW\n");
        exit(EXIT_FAILURE); 
    }

    glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // We want OpenGL 3.3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // We don't want the old OpenGL 

	m_window = glfwCreateWindow(width, height, name, NULL, NULL);

    if (m_window == NULL) 
    {
        fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n");
        glfwTerminate();
        exit(EXIT_FAILURE); 
    }
    
    glfwMakeContextCurrent(m_window);
    
    if (!gladLoadGL()) 
    {
        fprintf(stderr, "Failed to initialize GLAD\n");
        exit(EXIT_FAILURE); 
    }

    glfwSetInputMode(m_window, GLFW_STICKY_KEYS, GL_TRUE);
    glViewport(0, 0, width, height);
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    std::vector<glm::vec3>  positions =  { 
                                          glm::vec3(0.0f, 0.0f, 0.0f),     //0      2----------------3
                                          glm::vec3(0.5f, 0.0f, 0.0f),     //1                      
                                          glm::vec3(0.0f, 0.5f, 0.0f),     //2                        
                                          glm::vec3(0.5f, 0.5f, 0.0f),     //3      0-----------------1
                                          
                                          glm::vec3(0.5f, 0.0f, 0.0f),     //4      6-----------------7 
                                          glm::vec3(0.5f, 0.0f, -0.5f),    //5
                                          glm::vec3(0.5f, 0.5f, 0.0f),     //6
                                          glm::vec3(0.5f, 0.5f, -0.5f),    //7      4-----------------5
                                          
                                          glm::vec3(0.0f, 0.0f, -0.5f),    //8      10----------------11
                                          glm::vec3(0.5f, 0.0f, -0.5f),    //9
                                          glm::vec3(0.0f, 0.5f, -0.5f),    //10
                                          glm::vec3(0.5f, 0.5f, -0.5f),    //11     8-----------------9
                                          
                                          glm::vec3(0.0f, 0.0f, -0.5f),    //12     14----------------15
                                          glm::vec3(0.0f, 0.0f, 0.0f),     //13     
                                          glm::vec3(0.0f, 0.5f, -0.5f),    //14     
                                          glm::vec3(0.0f, 0.5f, 0.0f),     //15     12----------------13

                                          //Top 
                                          glm::vec3(0.0f, 0.5f,  0.0f),    //16     18----------------19
                                          glm::vec3(0.5f, 0.5f,  0.0f),    //17     
                                          glm::vec3(0.0f, 0.5f, -0.5f),    //18     
                                          glm::vec3(0.5f, 0.5f, -0.5f),    //19     16----------------17

                                          //Down                                   
                                          glm::vec3(0.0f, 0.0f,  0.0f),    //20     14----------------15
                                          glm::vec3(0.5f, 0.0f,  0.0f),    //21     
                                          glm::vec3(0.0f, 0.0f, -0.5f),    //22     
                                          glm::vec3(0.5f, 0.0f, -0.5f),    //23     12----------------13
                                                        
                                                                           };                                            

    std::vector<glm::vec4> colors =
    {
                                          glm::vec4(0.583f, 0.771f,   0.014f, 1.0f),     //0      2----------------3
                                          glm::vec4(0.609f,  0.115f,  0.436f, 1.0f),
                                          glm::vec4(0.327f,  0.483f,  0.844f, 1.0f),
                                          glm::vec4(0.822f,  0.569f,  0.201f, 1.0f),

                                          glm::vec4(0.583f, 0.771f,   0.014f, 1.0f),     //0      2----------------3
                                          glm::vec4(0.609f,  0.115f,  0.436f, 1.0f),
                                          glm::vec4(0.327f,  0.483f,  0.844f, 1.0f),
                                          glm::vec4(0.822f,  0.569f,  0.201f, 1.0f),

                                          glm::vec4(0.583f, 0.771f,   0.014f, 1.0f),     //0      2----------------3
                                          glm::vec4(0.609f,  0.115f,  0.436f, 1.0f),
                                          glm::vec4(0.327f,  0.483f,  0.844f, 1.0f),
                                          glm::vec4(0.822f,  0.569f,  0.201f, 1.0f),


                                          glm::vec4(0.583f, 0.771f,   0.014f, 1.0f),     //0      2----------------3
                                          glm::vec4(0.609f,  0.115f,  0.436f, 1.0f),
                                          glm::vec4(0.327f,  0.483f,  0.844f, 1.0f),
                                          glm::vec4(0.822f,  0.569f,  0.201f, 1.0f),
                                            
                                          glm::vec4(0.5f, 0.771f,   0.014f, 1.0f),     //0      2----------------3
                                          glm::vec4(0.609f,  0.115f,  0.436f, 1.0f),
                                          glm::vec4(0.327f,  0.483f,  0.844f, 1.0f),
                                          glm::vec4(0.822f,  0.569f,  0.201f, 1.0f),

                                          glm::vec4(0.583f, 0.771f,   0.014f, 1.0f),     //0      2----------------3
                                          glm::vec4(0.609f,  0.115f,  0.436f, 1.0f),
                                          glm::vec4(0.327f,  0.483f,  0.844f, 1.0f),
                                          glm::vec4(0.822f,  0.569f,  0.201f, 1.0f),

    }; 

    std::vector<glm::vec2> uvs =
    {
        glm::vec2(0.0f, 0.0f), glm::vec2(1.0f, 0.0f), glm::vec2(0.0f, 1.0f), glm::vec2(1.0f, 1.0f),
        glm::vec2(0.0f, 0.0f), glm::vec2(1.0f, 0.0f), glm::vec2(0.0f, 1.0f), glm::vec2(1.0f, 1.0f),
        glm::vec2(0.0f, 0.0f), glm::vec2(1.0f, 0.0f), glm::vec2(0.0f, 1.0f), glm::vec2(1.0f, 1.0f),
        glm::vec2(0.0f, 0.0f), glm::vec2(1.0f, 0.0f), glm::vec2(0.0f, 1.0f), glm::vec2(1.0f, 1.0f),
        glm::vec2(0.0f, 0.0f), glm::vec2(1.0f, 0.0f), glm::vec2(0.0f, 1.0f), glm::vec2(1.0f, 1.0f),
        glm::vec2(0.0f, 0.0f), glm::vec2(1.0f, 0.0f), glm::vec2(0.0f, 1.0f), glm::vec2(1.0f, 1.0f),
    };

    std::vector<GLuint> index = { 0, 1 ,2,  //Primer cuadrado 
                                  2, 1, 3,  
                                  4, 5, 6,  //Segundo cuadrado 
                                  6, 5, 7,   
                                  8, 9, 10,  //Tercer cuadrado 
                                  10, 9, 11, 
                                  12, 13,14,  //Cuarto cuadrado 
                                  14, 13,15, 
                                  16, 17, 18,  //Arriba cuadrado 
                                  18, 17, 19,   
                                  20, 21, 22,  //Abajo cuadrado 
                                  22, 21, 23
                                 }; 
     
    mesh = std::make_shared<Mesh>(positions, index, colors, uvs);

    shader = std::make_shared<ShaderProgram>("../../Assets/Shaders/BasicShader.vert", "../../Assets/Shaders/BasicShader.frag");

    cube = std::make_shared<Triangle>(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f), glm::vec3(1.0f, 1.0f, 1.0f));
    
    material = std::make_shared<Material>(*shader, *mesh, "Cube", *cube); 
    //triangle.reset(new Triangle(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f), glm::vec3(0.5f, 0.5f, 0.5f), positions, index, "data/Shaders/BasicShader.vert", "data/Shaders/BasicShader.frag"));
    camera = std::make_shared<Camera>(45.0f, (float)width / (float)height, 0.1f, 100.0f, false, glm::vec3(4, 3, 3), glm::vec3(0, 0, 0), true);
}

AlphaEngine::Window::~Window()
{
    delete m_window; 
}

void AlphaEngine::Window::Render()
{
    //Clear -> Render -> Swap -> Poll Event
    glEnable(GL_DEPTH_TEST); 
    glDepthFunc(GL_LESS);
    
    if(!glfwGetKey(m_window, GLFW_KEY_ESCAPE) && glfwWindowShouldClose(m_window) == 0)
    {
        // Clear the screen. It's not mentioned before Tutorial 02, but it can cause flickering, so it's there nonetheless.
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Draw nothing, see you in tutorial 2 ! 
        material->Render(); 
        // Swap buffers
        glfwSwapBuffers(m_window);
        //glfwPollEvents();
    }
}

GLFWwindow* AlphaEngine::Window::GetWindow()const
{
    return m_window;
}
