#include <Rendering/Mesh.hpp>
#include <Rendering/ShaderProgram.hpp>
#include <glad/glad.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <glm.hpp>
#include <Rendering/Triangle.hpp>
#include <Rendering/Material.hpp>
#include <Rendering/Camera.hpp>


AlphaEngine::Material::Material(AlphaEngine::ShaderProgram& shader, AlphaEngine::Mesh& firstMesh, std::string firstMeshName, Triangle &firstEntity) : m_shader(&shader)
{
	m_meshMap.insert(  std::pair<std::string, AlphaEngine::Mesh*>(    firstMeshName, &firstMesh));
	m_entityMap.insert(std::pair<std::string, AlphaEngine::Triangle*>(firstMeshName, &firstEntity)); 
}

AlphaEngine::Material::~Material()
{
}

void AlphaEngine::Material::Render()
{
	m_shader->UseProgram(); 

	//One batch 
	for(std::map<std::string, AlphaEngine::Mesh*>::iterator it = m_meshMap.begin(); it != m_meshMap.end(); it++)
	{
		//How to get model matrix? 
		glm::mat4 MVP = Camera::GetActiveCamera().GetPerspective() * Camera::GetActiveCamera().GetView() * m_entityMap[it->first]->GetModelMatrix();
		m_shader->SetMVP(MVP); 
		it->second->Draw(); 
	}

	m_shader->UnUseProgram(); 
}
