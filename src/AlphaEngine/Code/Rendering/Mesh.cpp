#include <iterator>
#include <gtc/matrix_transform.hpp>
#include <Rendering/Texture.hpp>
#include <Rendering/Mesh.hpp>


void AlphaEngine::Mesh::CreateBuffers()
{
	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	glGenBuffers(VBO_COUNT, m_vbo);

	//Quiero utilicar este buffer de datos 
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo[VBO_COORDINATES]);
	//Lo quiero rellenar con esta informacion 
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * m_positions.size(), m_positions.data(), GL_STATIC_DRAW);
	//Y quiero que parte del buffer corresponde a este atributo 
	glEnableVertexAttribArray(VBO_COORDINATES);
	glVertexAttribPointer(VBO_COORDINATES, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, m_vbo[VBO_COLOR]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec4) * m_color.size(), m_color.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(VBO_COLOR);
	glVertexAttribPointer(VBO_COLOR, 4, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, m_vbo[VBO_UVS]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * m_uvs.size(), m_uvs.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(VBO_UVS);
	glVertexAttribPointer(VBO_UVS, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vbo[VBO_INDEX]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * m_index.size(), m_index.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(VBO_INDEX);
	glVertexAttribPointer(VBO_INDEX, 3, GL_UNSIGNED_INT, GL_FALSE, 0, 0);

	glBindVertexArray(0);
}

AlphaEngine::Mesh::Mesh(std::vector<glm::vec3> vPositions, std::vector<GLuint>  vIndex, std::vector<glm::vec4> vColors, std::vector<glm::vec2> vUvs) :
m_positions(vPositions),
m_index(vIndex), 
m_color(vColors), 
m_uvs(vUvs),
texture("../../Assets/Textures/number_1.jpg", 1300, 1300, 0)
{
	CreateBuffers(); 
}

AlphaEngine::Mesh::~Mesh()
{ 
	glDeleteVertexArrays(1,         &m_vao); 
	glDeleteBuffers(     VBO_COUNT, m_vbo); 
}


void AlphaEngine::Mesh::Draw()
{
	glBindVertexArray(m_vao);
	glBindTexture(GL_TEXTURE_2D, texture.GetTextureID());
	glDrawElements(GL_TRIANGLES, m_index.size(), GL_UNSIGNED_INT, nullptr);
	glBindVertexArray(0); 
}
