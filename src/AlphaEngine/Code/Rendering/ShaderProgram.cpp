#include <string>
#include <iostream>
#include <sstream>
#include <iterator>
#include <fstream>
#include <sstream>
#include <vector>
#include <Rendering/Camera.hpp>
#include <gtc/type_ptr.hpp>
#include <Rendering/Mesh.hpp>
#include <Rendering/Triangle.hpp>
#include <Rendering/ShaderProgram.hpp>


AlphaEngine::ShaderProgram::ShaderProgram(const char* vertexShaderProgramPath, const char* fragShaderProgramPath)
{
	m_ID = CompileShaderProgram(vertexShaderProgramPath, fragShaderProgramPath);
	m_mvpID = glGetUniformLocation(m_ID, "MVP"); 
}

AlphaEngine::ShaderProgram::~ShaderProgram()
{
	glDeleteShader(m_ID); 
}

void AlphaEngine::ShaderProgram::UseProgram() const
{
	glUseProgram(m_ID); 
}

void AlphaEngine::ShaderProgram::UnUseProgram() const
{
	glUseProgram(0); 
}

void AlphaEngine::ShaderProgram::SetMVP(glm::mat4 mvp)
{
	glUniformMatrix4fv(m_mvpID, 1, GL_FALSE, glm::value_ptr(mvp)); 
}

GLuint AlphaEngine::ShaderProgram::GetID() const
{
	return m_ID; 
}

GLuint AlphaEngine::ShaderProgram::CompileShaderProgram(const char* vertexShaderProgramPath, const char* fragShaderProgramPath)
{
	GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragShaderID	  = glCreateShader(GL_FRAGMENT_SHADER); 
	GLuint ProgramID = 0; 

	GLint Result = GL_FALSE;
	int InfoLogLength;

	std::string vertexShaderCode, fragShaderCode;  

	CopyShaderCode(vertexShaderCode, vertexShaderProgramPath); 
	CopyShaderCode(fragShaderCode, fragShaderProgramPath); 
	
	// Compile Vertex Shader
	printf("Compiling shader : %s\n", vertexShaderProgramPath);
	CompileShader(vertexShaderID, vertexShaderCode.c_str()); 

	// Check Vertex Shader errors
	PrintShaderErrors(vertexShaderID, GL_COMPILE_STATUS, Result); 

	// Compile Fragment Shader
	printf("Compiling shader : %s\n", fragShaderProgramPath);
	CompileShader(fragShaderID, fragShaderCode.c_str()); 

	// Check Fragment Shader errors
	PrintShaderErrors(fragShaderID, GL_COMPILE_STATUS, Result);

	// Link the program
	printf("Linking program\n");
	CreateProgram(ProgramID, vertexShaderID, fragShaderID, Result); 
	return ProgramID;
}

void AlphaEngine::ShaderProgram::CompileShader(GLuint &shaderID, const char* shaderCodePointer)
{
	glShaderSource(shaderID, 1, &shaderCodePointer, NULL);
	glCompileShader(shaderID);
}

bool AlphaEngine::ShaderProgram::CopyShaderCode(std::string &shaderCodeContainer, const char* shaderCodePath)
{
	std::ifstream shaderStream(shaderCodePath, std::ios::in);

	if(shaderStream.is_open())
	{
		std::stringstream ss; 
		ss << shaderStream.rdbuf(); 
		shaderCodeContainer = ss.str(); 
		shaderStream.close(); 
		return true; 
	}
	else
	{
		std::printf("Couldn`t open path: \n", shaderCodePath); 
		getchar(); 
		return false;
	}
}

void AlphaEngine::ShaderProgram::CreateProgram(GLuint& programID, GLuint& vertexShaderID, GLuint& fragmentShaderID, GLint &result)
{
	programID = glCreateProgram();
	glAttachShader(programID, vertexShaderID);
	glAttachShader(programID, fragmentShaderID); 
	glLinkProgram (programID);

	PrintProgramErrors(programID, GL_LINK_STATUS, result); 

	glDetachShader(programID, vertexShaderID);
	glDetachShader(programID, fragmentShaderID); 

	glDeleteShader(vertexShaderID); 
	glDeleteShader(fragmentShaderID); 
}

void AlphaEngine::ShaderProgram::PrintShaderErrors(GLuint &shaderID, GLenum paramName, GLint &result)
{
	glGetShaderiv(shaderID, paramName, &result); 
	if(result == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &maxLength); 

		// The maxLength includes the NULL character
		std::vector<GLchar> errorLog(maxLength);
		glGetShaderInfoLog(shaderID, maxLength, &maxLength, &errorLog[0]);

		// Provide the infolog in whatever manor you deem best.
		std::copy(errorLog.begin(), errorLog.end(), std::ostream_iterator<GLchar>(std::cout, ""));

		// Exit with failure.
		glDeleteShader(shaderID); // Don't leak the shader.
		return; 
	}

	else
	{
		printf(" Shader Compilation Sucess \n");
		return; 
	}
}

void AlphaEngine::ShaderProgram::PrintProgramErrors(GLuint& programID, GLenum paramName, GLint& result)
{
	glGetProgramiv(programID, paramName, &result); 
	if(result == GL_FALSE)
	{
		GLint maxLength = 0; 
		glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &maxLength);

		std::vector<GLchar> errorLog(maxLength);
		glGetProgramInfoLog(programID, maxLength, &maxLength, &errorLog[0]); 

		std::copy(errorLog.begin(), errorLog.end(), std::ostream_iterator<GLchar>(std::cout, ""));
		glDeleteProgram(programID); 
		return; 
	}
	else
	{
		printf(" Link program sucess \n"); 
		return; 
	}

}



