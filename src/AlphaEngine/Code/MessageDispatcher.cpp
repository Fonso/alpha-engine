#include <iterator>
#include <Message.hpp>
#include <Observer.hpp>
#include <MessageDispatcher.hpp>

AlphaEngine::MessageDispatcher* AlphaEngine::MessageDispatcher::m_instance = nullptr; 

AlphaEngine::MessageDispatcher* AlphaEngine::MessageDispatcher::GetInstance() 
{
	if (m_instance == nullptr) 
	{
		m_instance = new AlphaEngine::MessageDispatcher(); 
	}
	return m_instance; 
};

void AlphaEngine::MessageDispatcher::BroadcastMessage(Message* message)
{
	//If anyone is listening to this message 
	for (std::map<Message*, std::vector<Observer*>>::iterator map_it = m_observerList.begin(); map_it != m_observerList.end(); ++map_it)
	{
		//If they values are the same. Broadcast that message 
		if (*message == *map_it->first)
		{
			for (std::vector<Observer*>::iterator it = m_observerList[map_it->first].begin(); it != m_observerList[map_it->first].end(); ++it)
			{
				DeliverMessage(message, **it);
			}
		}
	}
}

void AlphaEngine::MessageDispatcher::DeliverMessage(Message* message, const Observer& observer)
{
	message->HandleMsg(observer); 
}

void AlphaEngine::MessageDispatcher::RegisterObserver(Message* message, Observer& observer)
{
	//There is no key with the message name in the map 
	for (std::map<Message*, std::vector<Observer*>>::iterator map_it = m_observerList.begin(); map_it != m_observerList.end(); ++map_it)
	{
		if (*map_it->first == *message)
		{
			for (std::vector<Observer*>::iterator it = m_observerList[map_it->first].begin(); it != m_observerList[map_it->first].end(); ++it)
			{
				if (*it == &observer)
				{
					//If is already we dont add anything to the observer list
					
					return;
				}
			}
			//If we couldnt find the observer we must introduce it to the vector of observers
			map_it->second.push_back(&observer); 
			return; 
		}
	}

	std::vector<Observer*> new_obs_list; 
	new_obs_list.push_back(&observer); 
	m_observerList.insert(std::pair<Message*, std::vector<Observer*>>(message, new_obs_list));
};

void AlphaEngine::MessageDispatcher::UnRegisterObserver(Message* message, Observer& observer)
{
	for (std::map<Message*, std::vector<Observer*>>::iterator map_it = m_observerList.begin(); map_it != m_observerList.end(); ++map_it)
	{
		if (*map_it->first == *message)
		{
			for (std::vector<Observer*>::iterator it = m_observerList[map_it->first].begin(); it != m_observerList[map_it->first].end(); ++it)
			{
				if (*it == &observer)
				{
					//If is already in delete it from the list of observers
					it = m_observerList[message].erase(it);
					return;
				}
			}
		}
	}
};

