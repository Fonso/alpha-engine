#include<stdio.h>
#include <glm.hpp>
#include <Logging.hpp>
#include <Message.hpp>
#include <typeinfo>

namespace AlphaEngine
{
	int Message::m_id = 0; 

	const std::string &Message::GetMsg() const
	{
		return m_message;
	}

	void Message::HandleMsg(const IMessageHandler& observer)const
	{
		observer.HandleMessage(*this);
	}

	bool Message::operator ==(const Message& other)const 
	{
		if(this->m_message == "Key" && other.m_message == "Key")
		{
			KeyMessage new_other = static_cast<const KeyMessage&>(other);
			KeyMessage new_this =  static_cast<const KeyMessage&>(*this);
			return new_other == new_this; 
		}

		return this->m_message == other.m_message;
	}

	Message::Message(const char* msg) : m_message(msg)
	{
		m_id++; 
	}

	bool KeyMessage::operator ==(KeyMessage& other) const 
	{
		//Implement Multiple dispatch. See visitor pattern https://stackoverflow.com/questions/2169460/polymorphism-by-function-parameter
		return (this->m_keyCode == other.m_keyCode) && (this->m_keyState == other.m_keyState);
	}

	KeyMessage::KeyMessage(int keyCode, KeyMessage::KeyState state)
    : Message("Key"), m_keyCode(keyCode), m_keyState(state)
	{
	}

	void KeyMessage::HandleMsg(const IMessageHandler& observer)const 
	{
		observer.HandleMessage(*this);
	}

}


