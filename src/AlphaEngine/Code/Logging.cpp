#include <spdlog/spdlog.h>
#include <Logging.hpp>

namespace AlphaEngine
{
	std::shared_ptr <spdlog::logger>  Logging::m_CoreLogger;	
	std::shared_ptr <spdlog::logger>  Logging::m_ClientLogger;

	Logging::Logging()
	{
	}

	Logging::~Logging()
	{
	}

	void Logging::InitialiceLogger()
	{
		spdlog::set_pattern("%^ [%H:%M:%S %z] [%n] [---%L---] [thread %t] %v %$");
		Logging::m_CoreLogger   = spdlog::stdout_color_mt("Core Logger");
		Logging::m_CoreLogger->set_level(spdlog::level::trace);
		Logging::m_ClientLogger = spdlog::stderr_color_mt("App  Logger");
		Logging::m_CoreLogger->set_level(spdlog::level::trace);
	}

	inline std::shared_ptr<spdlog::logger>& Logging::GetCoreLogger()
	{
		return m_CoreLogger; 
	}

	inline std::shared_ptr<spdlog::logger>& Logging::GetClientLogger()
	{
		return m_ClientLogger; 
	}

	void Logging::GetOpenGLErrors()
	{
		GLenum err(glGetError());
		if (err)
		{
			std::string error;
			switch (err)
			{
			case GL_INVALID_OPERATION:  error = "INVALID_OPERATION";      break;
			case GL_INVALID_ENUM:       error = "INVALID_ENUM";           break;
			case GL_INVALID_VALUE:      error = "INVALID_VALUE";          break;
			case GL_OUT_OF_MEMORY:      error = "OUT_OF_MEMORY";          break;
			case GL_INVALID_FRAMEBUFFER_OPERATION:  error = "INVALID_FRAMEBUFFER_OPERATION";  break;
			}
			GetCoreLogger()->error ("GL_" + error);
		}
		else
		{
			GetCoreLogger()->info("No hay errores de OpenGL"); 
		}
	}
}


