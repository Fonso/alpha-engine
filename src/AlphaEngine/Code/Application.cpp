#include <iostream>
#include <Logging.hpp>
#include <Application.hpp>

namespace AlphaEngine
{
	Application::Application()
	{
		_window.reset(new Window(1000, 1000, "AlphaEngine Window"));
		m_eSystem.reset(new EventSystem(*_window->GetWindow())); 
	}

	Application::~Application()
	{
	}

	void Application::Run()
	{
		while(!exit)
		{ 
			//Read Input 
			m_eSystem->PollEvent(); 
			//Game Logic 
			this->Update(); 
			//Update Camera position  

			//World Update 

			//GUI Update 

			//AI Update 

			//Audio update 

			//Window Update 
			_window->Render(); 
		}
	}

	void Application::CreateMaterial()
	{

	}

}