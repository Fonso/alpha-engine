#include <Logging.hpp>
#include <Observer.hpp>

void AlphaEngine::Observer::HandleMessage(const Message& m)const 
{
	ALPHAENGINE_CORE_TRACE("HANDLING NORMAL MESSAGE"); 
}

void AlphaEngine::Observer::HandleMessage(const KeyMessage& m)const
{
	ALPHAENGINE_CORE_TRACE("HANDLING KEYMESSAGE");
}
