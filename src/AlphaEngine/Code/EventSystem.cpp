#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <Logging.hpp>
#include <Message.hpp>
#include <MessageDispatcher.hpp>
#include <EventSystem.hpp>

AlphaEngine::EventSystem::EventSystem(GLFWwindow& window) : m_window(&window)
{   
    SetGLFWCallbacks(); 
}

AlphaEngine::EventSystem::~EventSystem()
{
	delete m_window; 
}

void AlphaEngine::EventSystem::PollEvent()
{
	if( m_window && !glfwWindowShouldClose(m_window))
	{
		glfwPollEvents();
	}
}

void AlphaEngine::EventSystem::SetGLFWCallbacks()
{
    glfwSetKeyCallback(m_window, key_callback);
    glfwSetCharCallback(m_window, character_callback);
    glfwSetCursorEnterCallback(m_window, cursor_enter_callback);
    glfwSetCursorPosCallback  (m_window, cursor_position_callback);
    glfwSetMouseButtonCallback(m_window, mouse_button_callback);
}


//Posible actions 

void AlphaEngine::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    AlphaEngine::KeyMessage m(key, AlphaEngine::KeyMessage::KeyState::KEYDOWN);
    AlphaEngine::MessageDispatcher::GetInstance()->BroadcastMessage(&m); 
}; 

void AlphaEngine::character_callback(GLFWwindow* window, unsigned int codepoint)
{
}

void AlphaEngine::cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
}

void AlphaEngine::cursor_enter_callback(GLFWwindow* window, int entered)
{
}

void AlphaEngine::mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
}
