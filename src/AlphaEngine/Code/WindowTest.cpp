#include <WindowTest.hpp>
#include <Logging.hpp>

AlphaEngine::WindowTest::WindowTest(const int screen_width, const int screen_height, const char* title)
{
	glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4); // We want OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // We don't want the old OpenGL 

	if (!glfwInit())
	{
		ALPHAENGINE_CORE_CRITICAL("No se ha inicializado glfw correctamente");
		exit(EXIT_FAILURE);
	}

	_window = glfwCreateWindow(screen_width, screen_width, title, NULL, NULL);
	if (!_window)
	{
		glfwTerminate();
		ALPHAENGINE_CORE_CRITICAL("No se ha inicializado la variable _window");
	}
	glfwMakeContextCurrent(_window);
	if (!gladLoadGL())
	{	
		ALPHAENGINE_CORE_CRITICAL("Puta mierda");
		exit(EXIT_FAILURE); 
	}
	glfwSwapInterval(1);
}

AlphaEngine::WindowTest::~WindowTest()
{
	delete _window;
}

GLFWwindow& AlphaEngine::WindowTest::Get_GLFWWindow() const
{
	return *_window; 
}

void AlphaEngine::WindowTest::DestroySelf()
{
	glfwDestroyWindow(_window);
	glfwTerminate();
	ALPHAENGINE_CORE_INFO("Salida del engine con exito");
	exit(EXIT_SUCCESS);
}

void AlphaEngine::WindowTest::Render()
{
	if (!glfwWindowShouldClose(_window))
	{
		//Clear last frame buffer
		glClear(GL_COLOR_BUFFER_BIT);
		//Render new frame 

		//Swap buffers with the new frame 
		glfwSwapBuffers(_window);
		//Poll Events
		//glfwPollEvents();
	}
	else
	{
		DestroySelf(); 
	}
}
