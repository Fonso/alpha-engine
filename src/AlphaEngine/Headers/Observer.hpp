#pragma once

#include <Core.hpp>
#include <Message.hpp>

namespace AlphaEngine
{
	class ALPHA_API Observer : public IMessageHandler
	{
		public: 
			void HandleMessage(const Message& m)   const override;
			void HandleMessage(const KeyMessage& m)const override;
	};
}