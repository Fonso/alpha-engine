#pragma once
#include <Core.hpp>

namespace AlphaEngine
{
	class ALPHA_API EventSystem
	{
		public: 

		private:
			class GLFWwindow* m_window;

		public: 
			EventSystem(GLFWwindow &window); 
			~EventSystem();
			void PollEvent();

		private:
			void SetGLFWCallbacks(); 

	};

	//Keys part 
	void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
	void character_callback(GLFWwindow* window, unsigned int codepoint); 
	void cursor_position_callback(GLFWwindow* window, double xpos, double ypos); 
	void cursor_enter_callback(GLFWwindow* window, int entered); 
	void mouse_button_callback(GLFWwindow* window, int button, int action, int mods); 

	//Joysticks for future implementation 

}

