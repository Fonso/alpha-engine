#pragma once

#include <string.h>
#include <glad/glad.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <glm.hpp>
#include <Core.hpp>

namespace AlphaEngine
{
	/**
	TODO: Implement bump mapping http://www.opengl-tutorial.org/es/beginners-tutorials/tutorial-5-a-textured-cube/ 
	*/
	class ALPHA_API Texture
	{
		private: 
			GLuint m_textureID; 
			int m_width, m_height; 

		private: 
			void Bind(const char* TexturePath, int textureWidth, int textureHeight, int channel = 0);
		public: 
			Texture(const char* TexturePath, int textureWidth, int textureHeight, int channel); 
			GLuint& GetTextureID(); 
			virtual ~Texture(); 

	};
}


