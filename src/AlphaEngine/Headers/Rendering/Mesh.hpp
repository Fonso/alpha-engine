#pragma once
#include <glad/glad.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <glm.hpp>
#include <vector>
#include <Core.hpp>
#include <Rendering/Texture.hpp>

namespace AlphaEngine
{
	class ALPHA_API Mesh
	{
		protected:
			enum VBOType
			{
				VBO_COORDINATES,
				VBO_COLOR,
				VBO_UVS, 
				VBO_INDEX, 
				VBO_COUNT
			};

			GLuint m_vao; 
			GLuint m_vbo[VBO_COUNT]; 
			AlphaEngine::Texture texture;

			//TODO Do a class for this information
			std::vector<glm::vec3> m_positions;
			std::vector<glm::vec4> m_color; 
			std::vector<GLuint>    m_index; 
			std::vector<glm::vec2> m_uvs; 

		private: 
			void CreateBuffers(); 
		public:
			/** We want an array of this things to construct the mesh */
			Mesh(std::vector<glm::vec3> vPositions, std::vector<GLuint>  vIndex, std::vector<glm::vec4> vColors, std::vector<glm::vec2> vUvs);
			virtual ~Mesh(); 
			
			
			void Draw(); 


	};
}
