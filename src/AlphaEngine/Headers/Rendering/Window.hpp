#pragma once
#include <memory>
#include <glad/glad.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <Rendering/Triangle.hpp>
#include <Rendering/Material.hpp>
#include <Rendering/ShaderProgram.hpp>
#include <Rendering/Camera.hpp>
#include <Core.hpp>

namespace AlphaEngine
{
	class Mesh; 
	class ALPHA_API Window
	{

	private: 
		//We dont destroy the window so we dont need to create a shared pointer, we only need a raw one. 
		GLFWwindow* m_window; 
		std::shared_ptr<Material> material; 
		std::shared_ptr<Triangle> cube;  
		std::shared_ptr<Camera> camera; 
		std::shared_ptr<ShaderProgram>  shader;
		std::shared_ptr<Mesh> mesh; 


	public: 
		//Constructor
		Window(int width, int height, const char* name); 
		//Destructor
		virtual ~Window(); 

		void Render(); 

		GLFWwindow *GetWindow()const; 

	};
}

