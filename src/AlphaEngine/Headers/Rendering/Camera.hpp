#pragma once
#include <glad/glad.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <glm.hpp>
#include <Core.hpp>
#include <vector>

namespace AlphaEngine
{
	class ALPHA_API Camera
	{
		private: 
			float m_fov; 
			float m_ratio; 
			float m_near; 
			float m_far; 

			//TODO Make a constructor for ortographic cameras 
			bool m_ortographic; 
			/*
			*	It should only be a camera active at the same time 
			*/
			
			static Camera* m_activeCamera; 
			
			bool m_active; 

			glm::vec3 m_position;
			glm::vec3 m_rotation;

		public: 
			Camera(float fov, float ratio, float near, float far, bool isOrtographic, glm::vec3 position, glm::vec3 rotation, bool isActive);
			virtual ~Camera(); 
			inline bool isActive() const; 
			glm::mat4 GetPerspective(); 
			glm::mat4 GetView(); 
			static Camera& GetActiveCamera();

	};
}

