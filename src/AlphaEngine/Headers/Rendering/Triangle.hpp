#pragma once
#include <glad/glad.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <glm.hpp>
#include <memory>
#include <Rendering/ShaderProgram.hpp>
#include <Rendering/Mesh.hpp>
#include <Core.hpp>

namespace AlphaEngine
{
	class ALPHA_API Triangle
	{
		private:
			glm::vec3 m_position; 
			glm::vec3 m_rotation; 
			glm::vec3 m_scale; 

	public: 
		Triangle(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale);
		virtual ~Triangle(); 

		glm::mat4 GetModelMatrix(); 
	};
}

