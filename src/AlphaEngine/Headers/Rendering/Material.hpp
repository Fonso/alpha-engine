#pragma once
#include <memory>
#include <map>
#include <string>
#include <Core.hpp>


namespace AlphaEngine
{
	class ShaderProgram; 
	class Mesh; 
	class ALPHA_API Triangle; 
	//TODO Batch Rendering 
	class Material
	{
		private: 
			ShaderProgram*    m_shader; 
			std::map<std::string, Mesh*>      m_meshMap; 
			std::map<std::string, Triangle*>  m_entityMap; 
		public:
			Material(ShaderProgram &shader, Mesh &firstMesh, std::string firstMeshName, Triangle &firstEntity); 
			virtual ~Material(); 
			void Render(); 
	};
}

