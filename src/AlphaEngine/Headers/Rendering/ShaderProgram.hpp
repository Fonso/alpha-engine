#pragma once
#include <glad/glad.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <glm.hpp>
#include <string>
#include <Core.hpp>

namespace AlphaEngine
{
	class Triangle;
	class ALPHA_API ShaderProgram
	{
		private:
			GLuint m_ID; 

			GLuint m_mvpID;

		private: 
			GLuint  CompileShaderProgram(const char* vertexShaderProgramPath, const char* fragShaderProgramPath);

			void    CompileShader(GLuint &shaderID, const char* shaderCodePointer); 
			//TODO: Do it only with char 
			bool    CopyShaderCode(std::string &shaderCodeContainer, const char* shaderCodePath); 

			void    CreateProgram(GLuint &programID, GLuint &vertexShaderID, GLuint &fragmentShaderID, GLint &result); 

			void    PrintShaderErrors(GLuint &shaderID, GLenum paramName, GLint &result); 

			void	PrintProgramErrors(GLuint& programID, GLenum paramName, GLint& result);


		public:
			
			ShaderProgram(const char* vertexShaderProgramPath, const char* fragShaderProgramPath); 
			
			virtual ~ShaderProgram(); 

			void UseProgram() const; 
			
			void UnUseProgram()const; 

			void SetMVP(glm::mat4 mvp); 

			GLuint GetID() const; 

			


	};
}

