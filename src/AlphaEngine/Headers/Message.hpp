#pragma once
#include <string>
#include <map>
#include <Core.hpp>

namespace AlphaEngine
{
	class IMessageHandler; 

	class ALPHA_API Message
	{
		protected: 
			static int m_id; 
			std::string m_message;

		public:
			Message(const char* msg);
			//The reference is constant and the result is constant to(it cant be modified)
			const std::string &GetMsg()const;
			virtual void HandleMsg(const IMessageHandler &observer)const; 
			virtual bool operator == (const Message& other) const; 
	};

	class ALPHA_API KeyMessage : public Message
	{
		public:
			enum KeyState
			{
				KEYUP, KEYDOWN
			}
			m_keyState;

		private:
			int m_keyCode;

		public:
			KeyMessage(int keyCode, KeyState state);
			void HandleMsg(const IMessageHandler &observer)const;
			virtual bool operator ==(KeyMessage& other) const; 
	};

	class ALPHA_API IMessageHandler
	{
	public:
		virtual void HandleMessage(const Message& m) const = 0;
		virtual void HandleMessage(const KeyMessage& m) const = 0;
	};

}
