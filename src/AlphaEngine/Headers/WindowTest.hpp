#pragma once
#include <Core.hpp>
#include <memory>
#include <glad/glad.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

namespace AlphaEngine
{
	class ALPHA_API WindowTest
	{
	private: 
		GLFWwindow* _window; 

	public: 
		WindowTest(const int screen_width, const int screen_height, const char* title); 
		~WindowTest(); 
		GLFWwindow& Get_GLFWWindow() const; 
		void DestroySelf(); 
	public: 
		void Render(); 
	};
}

