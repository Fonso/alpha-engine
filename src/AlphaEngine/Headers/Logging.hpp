#pragma once
#include <memory>
#include <Core.hpp>
#include <spdlog/spdlog.h>
#include <glad/glad.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <Logging.hpp>
#include <string>

namespace AlphaEngine
{
	class ALPHA_API Logging
	{
		private:
			static std::shared_ptr <spdlog::logger> m_CoreLogger; 
		    static std::shared_ptr <spdlog::logger> m_ClientLogger; 

		public: 
			Logging(); 
			~Logging();
			static void InitialiceLogger(); 
			inline static std::shared_ptr<spdlog::logger> & GetCoreLogger  ();
			inline static std::shared_ptr<spdlog::logger> & GetClientLogger();
			static void GetOpenGLErrors(); 

	};


}

//Core macros
#define ALPHAENGINE_CORE_ERROR(...)::AlphaEngine::Logging::GetCoreLogger()->error       (__VA_ARGS__)  
#define ALPHAENGINE_CORE_WARN(...)::AlphaEngine::Logging::GetCoreLogger()->warn         (__VA_ARGS__)
#define ALPHAENGINE_CORE_INFO(...)::AlphaEngine::Logging::GetCoreLogger()->info         (__VA_ARGS__)
#define ALPHAENGINE_CORE_TRACE(...)::AlphaEngine::Logging::GetCoreLogger()->trace       (__VA_ARGS__)
#define ALPHAENGINE_CORE_CRITICAL(...)::AlphaEngine::Logging::GetCoreLogger()->critical (__VA_ARGS__) 
																					    
//Client macros																		    
#define ALPHAENGINE_ERROR(...)::AlphaEngine::Logging::GetClientLogger()->error          (__VA_ARGS__)  
#define ALPHAENGINE_WARN(...)::AlphaEngine::Logging::GetClientLogger()->warn            (__VA_ARGS__)
#define ALPHAENGINE_INFO(...)::AlphaEngine::Logging::GetClientLogger()->info            (__VA_ARGS__)
#define ALPHAENGINE_TRACE(...)::AlphaEngine::Logging::GetClientLogger()->trace          (__VA_ARGS__)
#define ALPHAENGINE_CRITICAL(...)::AlphaEngine::Logging::GetClientLogger()->critical    (__VA_ARGS__) 

//OpenGL macros
#define OPENGL_ERRORS  AlphaEngine::Logging::GetOpenGLErrors()