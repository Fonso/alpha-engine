#pragma once
#include <Core.hpp>
#include <map>
#include <queue>
#include <string>

namespace AlphaEngine
{
	class ALPHA_API MessageDispatcher
	{
		private: 
			std::map<class Message*, std::vector<class Observer*>> m_observerList; 
		
			MessageDispatcher() {};

			static MessageDispatcher* m_instance;  
		public: 
			static MessageDispatcher* GetInstance(); 

			MessageDispatcher(MessageDispatcher& other) = delete; 
			void operator = (const MessageDispatcher& other) = delete;

			void BroadcastMessage(class Message *m); 
			void DeliverMessage(Message* m, const Observer& observer); 
			void RegisterObserver(Message* messageName, Observer &observer); 
			void UnRegisterObserver(Message* messageName, Observer& observer); 
	};
}


