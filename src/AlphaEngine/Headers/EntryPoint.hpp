#pragma once
#include <iostream>
#include <Application.hpp>
#include <WindowTest.hpp>

#ifdef ALPHA_PLATFORM_WINDOWS

extern AlphaEngine::Application* CreateAplication(); 

int main(int argc, char** argv)
{
	std::cout << " Alpha Engine Working " << "\n";
	//Creating the object for our application/engine to run. 
	std::shared_ptr<AlphaEngine::Application> app(CreateAplication());
	//Initialicing the logging system.
	AlphaEngine::Logging::InitialiceLogger();
		
	app->Start(); 
	//Main loop of the application. 
	app->Run();
	//If the aplication stops running, we destroy application and free the memory used.  
	app.reset(); 
}
#endif
