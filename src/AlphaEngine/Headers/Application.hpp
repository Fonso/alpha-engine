#pragma once
#include <vector>
#include <memory>
#include <Core.hpp>
#include <EventSystem.hpp>
#include <Rendering/Window.hpp>

namespace AlphaEngine
{
	class  ALPHA_API Application
	{
		private:
			bool exit = false; 
			std::shared_ptr<AlphaEngine::Window> _window; 
			std::shared_ptr<AlphaEngine::EventSystem> m_eSystem; 

		public: 
			Application(); 
			virtual ~Application();
			
			//To be defined by the person who use the engine.  
			Application* CreateAplication(); 
			virtual void Start() { };
			virtual void Update(){ }; 
			//Main loop of the app 
			void Run();

			void CreateMaterial(); 

			void StopApplication()
			{
				exit = true; 
			}
	};

	

}