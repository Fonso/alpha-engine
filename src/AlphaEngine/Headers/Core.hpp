#pragma once

#ifdef ALPHA_PLATFORM_WINDOWS
	#ifdef ALPHA_BUILD_DLL
		#define ALPHA_API _declspec(dllexport) 
	#else
		#define ALPHA_API _declspec(dllimport)
	#endif
#else 
	#error Alpha only supports Windows
#endif

