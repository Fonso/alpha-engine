#version 330 core 

//Uniform 
uniform sampler2D basicTexture; 

//IN 
in vec4 fragmentColor; 

//OUT 
out vec4 color; 
in  vec2 uv; 


void main()
{
	color = texture(basicTexture ,uv) * fragmentColor; 
}



