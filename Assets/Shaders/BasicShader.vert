#version 330 core
layout(location = 0) in vec3 modelPosition; 
layout(location = 1) in vec4 vertexColor; 
layout(location = 2) in vec2 texCoordinate; 


//UNIFORMS
//Model view projection
uniform mat4 MVP; 

//OUT 
out vec4 fragmentColor; 
out vec2 uv; 

void main()
{
	fragmentColor = vertexColor;
	uv		      = texCoordinate; 
	gl_Position = MVP * vec4(modelPosition, 1.0); 
}

